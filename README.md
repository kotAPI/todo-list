 Figma - https://www.figma.com/file/6SDoXGM96yiD9fTf3seu9N/Frontend-assignement?node-id=1%3A3
 Netlify Demo Link - https://kotapi-todo.netlify.app/

 ## Features Built

#### Home Page - 
All todos section displays 3 todos sorted by not completed tasks
Assigned To you section lists all the tasks assigned to you (Michael Scott)
Reminders show todos assigned to you and have a due date greater than today
Created by you section shows todos created by you(Michael Scott)
Routines Section - I didn't quite get what routines do from the figma design so it just displays todos created by you(Michael Scott)
Create Group Section - I didn't grasp what a "Group" meant from the Figma design so it just acts as a dummy button that takes you to full todo-list page

#### Todos Page /todos
Todos page lists all the todos that have been created
Supports Pagination to 4 todos per page (Display per-page can be changed from /api/todos.js)
Supports Marking as complete from the page itself (Click on the task icon to the left of todo to mark as complete, followed by a data refresh)
New Todos can be added by clicking the add button
Existing todos can be updated by clicking on the edit button on the action tab of the task
New Todo and Edit todo use the same react component (AddTodoModal)

#### Total time spent on the app
I've completed the basic app in about 3-4 hours, spent another 3-4 or so hours polishing the UI and refactoring the code in parts where I'm not happy with the components - which I found I was repeating myself. 

#### Tests
I've added some tests to a few components to demonstrate the testability of them, you can run tests by running the following command. I've added about 10 tests.
```
yarn test
```
or 
```
npm run test
```

#### Starter Code
I've created the starter code by using create-react-app

#### Regarding Following of Design Docs
I've tried building the app as closely to the figma design as possible, in places where things weren't clear to me, I've used my own ways of implementing things to finish off the functionality and not get into the details

#### Hosting
I used Netlify to deploy the react code - Demo URL can be accessed here - https://kotapi-todo.netlify.app/

