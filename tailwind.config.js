module.exports = {
    content: [
        "./src/**/*.{js,jsx,ts,tsx}",
    ],
    theme: {
        extend: {
            colors: {
                'cyan': {
                    100: '#cffafe',
                    200: '#a5f3fc',
                    300: '#67e8f9',
                    400: '#22d3ee',
                    500: '#06b6d4',
                    600: '#0891b2',
                    700: '#0e7490',
                    800: '#155e75',
                    900: '#164e63',
                },
                // ...
                'purple': {
                    100: '#F8DAFF',
                    200: '#E7C0FF',
                    300: '#DE59FF',
                    400: '#9E00FF',
                    500: '#510283'
                }
            },
        },
    },
    plugins: [],
}