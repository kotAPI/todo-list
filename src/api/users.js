const currentUser = {
    "id": "5e93b4a43af44260882e33b0",
    "firstname": "Michael",
    "lastname": "Scott",
    "avatar": "https://jeprades.github.io/michael-scott-tribute/images/michael-scott-header.png",

}

const otherUsers = [
    {
        "id": "5e93b4f03af44260882e33b1",
        "firstname": "Jim",
        "lastname": "Halpert",
        "avatar": "https://pbs.twimg.com/profile_images/3171824697/ef75d90df2e65ce326acf30262df5918_400x400.jpeg"
    },
    {
        "id": "5e93b4fa3af44260882e33b2",
        "firstname": "Dwight",
        "lastname": "Schrute",
        "avatar": "https://jeprades.github.io/michael-scott-tribute/images/dwight-schrute.png"
    },
    {
        "id": "5e93b50a3af44260882e33b3",
        "firstname": "Pam",
        "lastname": "Beesly",
        "avatar": "https://i.pinimg.com/474x/4c/51/96/4c51969054c87cbe670b85a830c3b136.jpg",
    },
    {
        "id": "5e93b5183af44260882e33b4",
        "firstname": "Ryan",
        "lastname": "Howard",
        "avatar": "https://jeprades.github.io/michael-scott-tribute/images/ryan%20howard.png",
    },
    {
        "id": "5e93b51e3af44260882e33b5",
        "firstname": "Kelly",
        "lastname": "Kapoor",
        "avatar": "https://images.entertainment.ie/uploads/2019/01/24110225/Kelly-Kapoor.jpg?w=640&h=384&q=high",
    }
]

export const getOtherUsers = () => {
    return otherUsers
}

export const getCurrentUser = () => {
    return currentUser
}

export const getAllUsers = () => {
    return [getCurrentUser()].concat(getOtherUsers())
}
