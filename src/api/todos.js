import { v4 as uuidv4 } from 'uuid';

const { getCurrentUser } = require("./users")
let todos = []
const PER_PAGE = 4;


export const getPerPage = () => {
    return PER_PAGE
}

export const getTotalAvailablePages = () => {
    return Math.ceil(getTodos().length / PER_PAGE)
}

export const getTotalTodoCount = () => {
    return getTodos().length;
}

export const getPageTodos = (currentPage = 0) => {
    if (todos.length === 0 && localStorage.todos) {
        todos = JSON.parse(localStorage.todos)
    }


    let leftIndex = currentPage * PER_PAGE;
    let rightIndex = leftIndex + PER_PAGE;

    return todos.slice(leftIndex, rightIndex)
}


export const getTodos = () => {
    if (todos.length === 0 && localStorage.todos) {
        todos = JSON.parse(localStorage.todos)
    }
    return todos
}

export const addToDoItem = (todoItem) => {
    todoItem.id = uuidv4();
    todoItem.created_by = getCurrentUser()
    todos.push(todoItem)
    sortAndSyncToDos();
}


const sortAndSyncToDos = () => {
    let todoList = JSON.parse(JSON.stringify(todos))
    todoList.sort((x, y) => {
        return Number(x.completed) - Number(y.completed)
    });

    todos = todoList
    syncTodos()
}

export const deleteTodoItem = (todoItem) => {
    let todoList = JSON.parse(JSON.stringify(todos))
    let index = todoList.findIndex(todo => {
        return todo.id === todoItem.id
    })

    if (index > -1) {
        todoList.splice(index, 1)
    }

    todoList.sort((x, y) => {
        return Number(x.completed) - Number(y.completed)
    });

    todos = todoList
    syncTodos()
}

export const toggleTodoCompleteStatus = (todoItem) => {
    let updatedTodoItem = JSON.parse(JSON.stringify(todoItem))
    updatedTodoItem.completed = !updatedTodoItem.completed
    updateTodoItem(updatedTodoItem)
    return todoItem
}

const syncTodos = () => {
    let string = JSON.stringify(todos);
    localStorage.todos = string;
}

export const updateTodoItem = (todoItem) => {
    let todoList = JSON.parse(JSON.stringify(todos))
    let index = todoList.findIndex(todo => {
        return todo.id === todoItem.id
    })
    if (index > -1) {
        todoList[index] = todoItem
    }
    // sort todolist by completed
    todoList.sort((x, y) => {
        return Number(x.completed) - Number(y.completed)
    });

    todos = todoList
    syncTodos()
}


/**
 * Dashboard Mock APIs
 */

export const getDashboardTodos = () => {
    let todos = getTodos();
    let briefTodos = todos;
    briefTodos = briefTodos.slice(0, 3)

    return {
        count: todos.length,
        todos: briefTodos
    }
}

export const createdByYouTodos = () => {
    let todos = getTodos();
    let currentUser = getCurrentUser()
    let briefTodos = todos.filter(todo => {
        return todo.created_by.id === currentUser.id
    });
    briefTodos = briefTodos.slice(0, 3)

    return {
        count: briefTodos.length,
        todos: briefTodos
    }
}

export const todosAssignedToMe = () => {
    let todos = getTodos();
    let currentUser = getCurrentUser()
    let briefTodos = todos.filter(todo => {
        return todo.assigned_to === currentUser.id
    });
    briefTodos = briefTodos.slice(0, 3)

    return {
        count: briefTodos.length,
        todos: briefTodos
    }
}

export const getDueTodos = () => {
    let todos = getTodos();
    let currentUser = getCurrentUser()
    let briefTodos = todos.filter(todo => {
        return todo.assigned_to === currentUser.id
    });

    briefTodos = briefTodos.filter(todo => {
        if (todo.due_date) {
            let time = new Date(todo.due_date).getTime()
            return time >= new Date().getTime()
        }

        return false

    })

    briefTodos = briefTodos.slice(0, 3)

    return {
        count: briefTodos.length,
        todos: briefTodos
    }
}