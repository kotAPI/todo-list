import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.scss';


import reportWebVitals from './reportWebVitals';

import NavBar from "./components/NavBar/NavBar"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

/// PAGES
import Home from "./pages/Home/Home"
import ToDos from './pages/ToDos/ToDos';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>

    <Router>
      <NavBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/todos" element={<ToDos />} />
      </Routes>
    </Router>
  </React.StrictMode>
);




// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
