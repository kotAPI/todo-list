import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import "../../../style/addTodoModal.scss"
import PlusLogo from "../../../assets/plus.svg"
import InfoIcon from "../../../assets/info.svg"

const { addToDoItem, updateTodoItem, toggleTodoCompleteStatus } = require("../../../api/todos")
const { getAllUsers } = require("../../../api/users")

const initTodo = {
    name: "",
    priority: "LOW",
    due_date: undefined,
    assigned_to: undefined,
    completed: false
}

const AddToDoModal = function({ toggleOffTodo, handleTodoChange, editTodo, handleDeleteTodo }) {

    const navigate = useNavigate();

    const [isEditing, setIsEditing] = useState(false);
    const [confirmToggled, setConfirmToggled] = useState(false);

    const [todo, setTodo] = useState(initTodo);
    const [users, setUsers] = useState(getAllUsers())


    useEffect(() => {
        if (editTodo) {
            setIsEditing(true)
            setTodo(editTodo)
        } else {
            setTodo(initTodo)
        }
    }, []);

    /**
     * Input Handlers
     */
    const handleTodoNameChange = (e) => {
        setTodo((prevState) => ({
            ...prevState,
            name: e.target.value
        }));
    }
    const handlePriorityChange = (e) => {
        setTodo((prevState) => ({
            ...prevState,
            priority: e.target.value
        }));
    }
    const handleDueDateChange = (e) => {
        setTodo((prevState) => ({
            ...prevState,
            due_date: e.target.value
        }));
    }
    const handleAssignedToChange = (e) => {
        setTodo((prevState) => ({
            ...prevState,
            assigned_to: e.target.value
        }));
    }
    // end input handlers

    const handleCancelTodo = () => {
        toggleOffTodo()
    }

    // 


    const handleTodoCompleteStatus = () => {

        let todoItem = toggleTodoCompleteStatus(todo)

        setTodo((prevState) => ({
            ...prevState,
            completed: todoItem.completed
        }));

        handleTodoChange()
    }



    const handleAddOrUpdateTodo = () => {
        if (isEditing) {
            updateTodoItem(todo)
            handleTodoChange()
        } else {
            addToDoItem(todo)
            handleTodoChange()
        }

    }

    const handleDeleteClick = () => {
        setConfirmToggled(true)
    }
    const confirmCancelTodo = () => {
        handleDeleteTodo(todo);
        toggleCancelConfirmOff();
    }
    const toggleCancelConfirmOff = () => {
        setConfirmToggled(false)
    }



    const DeleteSetings = () => {
        return (
            <div className='flex items-center justify-between mt-2'>
                <div className='font-medium'>
                    Do you want to delete this todo?
                </div>
                <button onClick={handleDeleteClick} className="bg-purple-400 text-white px-4 text-xs font-medium py-2 border-b-2 border-purple-500 hover:bg-purple-500 rounded-xl flex items-center"> <span>Delete todo</span></button>
            </div>
        )
    }

    const ConfirmDeleteAlert = () => {
        return (
            <div className='p-10 shadow-lg border border-gray-100 mt-4 w-2/3'>
                <div className='flex items-center'>
                    <img src={InfoIcon} width="20" alt="" />
                    <p className='text-sm font-medium ml-2'>Are you sure you want to delete this todo? This action is irreversible</p>
                </div>
                <div className='flex justify-end mt-4'>
                    <button onClick={toggleCancelConfirmOff} className="bg-gray-200 text-black px-4  text-xs font-medium py-2 mr-2 border-b-2 border-gray-300 rounded-xl flex hover:bg-gray-300 items-center" >No</button>

                    <button onClick={confirmCancelTodo} className="bg-purple-400 text-white px-3  text-xs font-medium py-2 border-b-2 border-purple-500 hover:bg-purple-500 rounded-xl flex items-center">  <span>Yes</span></button>
                </div>
            </div>
        )
    }

    const DeleteTodo = (task) => {

        return (
            <>
                {confirmToggled ? <ConfirmDeleteAlert /> : <DeleteSetings />}
            </>

        )
    }


    const TodoActions = () => {
        return (
            <div>
                <div className='border-b border-gray-200 mt-10'></div>
                <div className='flex items-center justify-between mt-2'>
                    <div className='font-medium'>
                        Mark this todo complete
                    </div>
                    <button onClick={handleTodoCompleteStatus} className="bg-purple-400 text-white px-4 text-xs font-medium py-2 border-b-2 border-purple-500 hover:bg-purple-500 rounded-xl flex items-center"> <span>Mark {todo.completed ? 'Incomplete' : 'Complete'} </span></button>
                </div>
                {/*  */}
                <div className='border-b border-gray-200 mt-10'></div>
                {DeleteTodo()}
            </div>
        )
    }


    const AddOrUpdateButton = () => {
        return (

            <button onClick={handleAddOrUpdateTodo} className="bg-purple-400 text-white px-3 text-xs font-medium py-2 border-b-2 border-purple-500 hover:bg-purple-500 rounded-xl flex items-center">  <span>{isEditing ? 'Save Changes' : (<div className='flex items-center'>
                <img src={PlusLogo} width="12" className='mr-1' /><span className='inline-block pr-2'>Add</span>
            </div>)}</span></button>

        )
    }

    return (
        <>
            <div className='w-screen h-screen absolute top-0 left-0 add-todo-modal-bg '>
            </div>
            <div className='bg-white absolute right-0 top-0 h-screen w-full lg:w-1/2 p-10 add-todo-modal-main overflow-y-auto'>
                <h2 className='font-bold text-lg mb-10'> {isEditing ? 'Edit' : 'Add'} Todo</h2>

                <div className='mb-4'>
                    <label className='text-sm text-gray-600 mb-2'>Todo</label>
                    <input onChange={handleTodoNameChange} value={todo.name} type="text" className='border outline-none px-2 py-1 border-gray-200  text-sm w-full' placeholder='Type here' />
                </div>
                {/*  */}
                <div className='mb-4'>
                    <label className='text-sm text-gray-600 mb-2'>Priority</label>
                    <select onChange={handlePriorityChange} value={todo.priority} className='w-full block border border-gray-200 px-2 py-1'>
                        <option value="LOW">Low</option>
                        <option value="MEDIUM">Medium</option>
                        <option value="HIGH">High</option>
                    </select>
                </div>
                <div className='mb-4'>
                    <label className='text-sm text-gray-600 mb-2'>Due Date</label>
                    <input value={todo.due_date} onChange={handleDueDateChange} type="date" className='border outline-none px-2 py-1 border-gray-200  text-sm w-full' />
                </div>
                <div className='mb-4'>
                    <label className='text-sm text-gray-600 mb-2'>Assigned To</label>
                    <select onChange={handleAssignedToChange} value={todo.assigned_to} className='w-full block border border-gray-200 px-2 py-1'>
                        <option value=''></option>
                        {users.map((user) => {
                            return <option value={user.id} key={user.id}> {user.firstname}</option>
                        })}
                    </select>
                </div>
                {/*  */}
                <div className='flex items-center justify-between'>
                    <button onClick={handleCancelTodo} className="bg-gray-200 text-black px-4  text-xs font-medium py-2 border-b-2 border-gray-300 rounded-xl flex hover:bg-gray-300 items-center" >Cancel</button>

                    <AddOrUpdateButton />

                </div>
                {/*  */}
                {isEditing ? <TodoActions /> : ''}

            </div>



        </>
    )
}




export default AddToDoModal;
