import "../../style/pageStyles.scss"
import DetailedTodo from "../../components/TodoItems/DetailedTodo"
import AddToDoModal from './Helpers/AddTodoModal';
import { useEffect, useState } from 'react';
import Pagination from '../../components/UI/Pagination';
import { deleteTodoItem, getTotalAvailablePages, getTotalTodoCount } from "../../api/todos"

import PlusLogo from "../../assets/plus.svg"

const { getPageTodos, toggleTodoCompleteStatus } = require("../../api/todos")

const ToDos = function() {


    const [addToDoToggled, setAddToDoToggled] = useState(false);
    const [editTodo, setTodoToEdit] = useState(undefined);
    const [allTasks, setAllTasks] = useState([])
    const [page, setPage] = useState(0)
    const [maxPages, setMaxPages] = useState(getTotalAvailablePages())
    const [totalItems, setTotalItems] = useState(getTotalTodoCount())

    useEffect(() => {
        setAllTasks(getPageTodos(page))
    }, [page])

    const handleTodoChange = () => {
        setAllTasks(getPageTodos(page))
        toggleOffTodo()
    }

    const toggleTodo = () => {
        setAddToDoToggled(true)
    }

    const toggleOffTodo = () => {
        setAddToDoToggled(false)
        setTodoToEdit(undefined)
    }

    const handleTodoEdit = (task) => {
        setTodoToEdit(task)
        toggleTodo()
    }

    const toggleTodoCompletion = (task) => {
        toggleTodoCompleteStatus(task)
        setAllTasks(getPageTodos(page))
    }

    const handleDeleteTodo = (task) => {
        deleteTodoItem(task)
        toggleOffTodo()
        handleTodoChange();
    }

    const pageUpHandler = () => {
        let currentPage = page;
        currentPage += 1;
        if (currentPage >= maxPages - 1) {
            currentPage = maxPages - 1
        }

        setPage(currentPage)
    }

    const pageDownHandler = () => {
        let currentPage = page;
        currentPage -= 1;
        if (currentPage < 0) {
            currentPage = 0
        }

        setPage(currentPage)
    }

    return (
        <div>
            {addToDoToggled ? <AddToDoModal handleDeleteTodo={handleDeleteTodo} editTodo={editTodo} handleTodoChange={handleTodoChange} toggleOffTodo={toggleOffTodo} /> : ''}

            <div className='page pt-20'>
                <div className=' w-full lg:w-4/5 bg-white mx-auto p-5 rounded-2xl'>
                    <div className='flex items-center justify-between'>
                        <h2 className='font-bold text-lg pb-2 '>All todos</h2>

                        <button onClick={toggleTodo} className="bg-purple-400 text-white px-2 pr-4 text-sm font-medium py-2 border-b-2 border-purple-500 rounded-xl flex items-center"> <img src={PlusLogo} width="12" className='mr-1' alt="plus" /> <span>Add</span></button>

                    </div>
                    <table className="table-auto w-full text-left text-sm my-5">
                        <thead>

                            <tr className='text-gray-400 border-b border-gray-200 '>
                                <th className='font-normal pl-7 pb-2'>Todo</th>
                                <th className='font-normal'>Priority</th>
                                <th className='font-normal'>Due Date</th>
                                <th className='font-normal'>Created by</th>
                                <th className='font-normal'>Assigned  to</th>
                                <th className='font-normal'>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            {allTasks.map((task) => {
                                return (<DetailedTodo toggleTodoCompletion={toggleTodoCompletion} handleTodoEdit={handleTodoEdit} task={task} key={task.id} />)
                            })}
                        </tbody>
                    </table>
                    <div>
                        <Pagination totalItems={totalItems} currentPage={page} totalPages={maxPages} pageDown={pageDownHandler} pageUp={pageUpHandler} />
                    </div>
                </div>

            </div>

        </div>
    )
}
export default ToDos;