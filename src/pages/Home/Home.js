import "../../style/home.scss"
import "../../style/pageStyles.scss"
import AllTodos from "../../components/AllTodos/AllTodos"
import AddProject from "../../components/AddProject/AddProject"
import AssignedToYouAndReminders from "../../components/AssignedToYouAndReminders/AssignedToYouAndReminders"
import CreatedByYouAndRoutines from "../../components/CreatedByYouAndRoutines/CreatedByYouAndRoutines"

const Home = function() {
    return (
        <>
            <div className='page'>
                <div className="scrolling-wrapper text-sm lg:text-normal">
                    <AllTodos />
                    <AssignedToYouAndReminders />
                    <CreatedByYouAndRoutines />
                    <AddProject />
                </div>
            </div>
        </>
    )
}
export default Home;

/**
 * Links used -
 * 
 * Horizontal scrolling containers -  https://codeburst.io/how-to-create-horizontal-scrolling-containers-d8069651e9c6
 */