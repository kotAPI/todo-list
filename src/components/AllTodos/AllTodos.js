import React from 'react';
import CompactTasksRenderer from "../CompactTasksRenderer/CompactTasksRenderer"
let { getDashboardTodos } = require("../../api/todos")

function AllTodos() {
    const todoRes = getDashboardTodos()
    return (
        <div className='card'>
            <div className='container'>
                <h2 className='font-bold text-lg pb-2 border-b border-gray-200'>All todos</h2>
                <CompactTasksRenderer tasksObject={todoRes} showDetails={true} />
            </div>

        </div>
    );
}
export default AllTodos