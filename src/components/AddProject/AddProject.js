import React from 'react';
import { useNavigate } from 'react-router-dom'
import PlusPurpleIcon from "../../assets/plus-purple.svg"
function AddProjectGroup() {
    const navigate = useNavigate();

    function goToTodos() {
        navigate("/todos");
    }

    return (

        <>
            <div className='card cursor-pointer ' onClick={goToTodos} data-testid="add-project">
                <div className='container flex items-center justify-center w-full h-60'>
                    <div>
                        <img src={PlusPurpleIcon} alt="Plus Purple Icon" />
                        <p className='text-purple-300 mt-4 text-xs text-center'> Create Group</p>
                    </div>

                </div>
            </div>
        </>
    );
}



export default AddProjectGroup