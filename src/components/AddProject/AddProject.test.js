import { render, screen } from '@testing-library/react';
import Component from './AddProject';
import { BrowserRouter as Router } from 'react-router-dom';
import "@testing-library/jest-dom/extend-expect";
// 👇️ must wrap your component in <Router>

test('Handles no props gracefully without erroring out', async () => {
    const { getByTestId } = render(
        <Router>
            <Component />,
        </Router>,
    );

    const elem = getByTestId("add-project");
    expect(elem).toBeInTheDocument();
});

test('Renders the AddProject Component', async () => {
    render(
        <Router>
            <Component />,
        </Router>,
    );
    const elem = screen.getByText('Create Group').closest('div')
    expect(elem).toBeInTheDocument()
});

test('Shows text "Create Group" in the container', async () => {
    render(
        <Router>
            <Component />,
        </Router>,
    );
    const elem = screen.getByText('Create Group').closest('div')
    expect(elem).toBeInTheDocument()
});

test('Displays Image Element', async () => {
    const { getByAltText } = render(
        <Router>
            <Component />,
        </Router>,
    );
    const image = getByAltText('Plus Purple Icon');
    expect(image).toBeInTheDocument()
});