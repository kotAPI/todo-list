import React from 'react';
import Avatar from '../UI/Avatar';
import LabelRenderer from '../UI/LabelRenderer';
import TaskIcon from '../UI/TaskIcon';
import EditIcon from "../../assets/edit.svg"
const { getAllUsers } = require("../../api/users")

function CompactTodo({ task, handleTodoEdit, toggleTodoCompletion }) {

    const handleEditTodo = () => {
        handleTodoEdit(task)
    }

    const formatDate = (date) => {
        if (date) {
            var options = { month: 'short', day: 'numeric', year: 'numeric' };
            let dateObj = new Date(date)
            return dateObj.toLocaleDateString("en-US", options)

        }
        return "None"
    }

    const toggleTaskCompletion = () => {
        toggleTodoCompletion(task)
    }


    const AssignedTo = ({ assigned }) => {
        let users = getAllUsers();
        let user = users.find(user => {
            return user.id === assigned
        })
        return (
            <Avatar user={user} size={28} />
        )
    }

    const doneStyle = {
    }
    if (task.completed) {
        doneStyle.color = "#c3bebe"
        doneStyle.fontSize = "14px"
        doneStyle.fontWeight = "100"
    }

    return (
        <tr className='py-2 border-b border-gray-200'>
            <td className='font-medium'>
                <div className='flex pt-2 pb-2'>
                    <TaskIcon toggleTaskCompletion={toggleTaskCompletion} completed={task.completed} />
                    <div className='ml-2' style={doneStyle} >
                        {task.name}
                    </div>
                </div>
            </td>
            <td> <LabelRenderer priority={task.priority} /></td>
            <td className='font-medium' >{formatDate(task.due_date)}</td>
            <td className='pl-4 pt-2'> <Avatar size={28} user={task.created_by} /> </td>
            <td><AssignedTo assigned={task.assigned_to} /></td>
            <td> <button onClick={handleEditTodo} className="text-gray-500 flex items-center  text-sm font-medium py-2 rounded-xl flex hover:text-gray-700 items-center" >
                <span className='pl-2'>Edit</span><img className='pl-4' src={EditIcon} alt="" /></button></td>
        </tr>

    );
}

export default CompactTodo