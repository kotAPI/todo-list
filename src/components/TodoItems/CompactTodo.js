import React from 'react';
import Avatar from "../../components/UI/Avatar"
import LabelRenderer from "../../components/UI/LabelRenderer"
import TaskIcon from "../../components/UI/TaskIcon"

function CompactTodo({ task, showDetails }) {
    return (

        <div className='flex my-4 text-sm'>
            <TaskIcon completed={task.completed} />
            <div className='ml-2 w-full'>
                <div className='overflow-hidden truncate font-medium'>{task.name}</div>

                {showDetails ?
                    <div className='flex items-center justify-between w-full'>
                        <div > <LabelRenderer priority={task.priority} /></div>
                        <div className='flex items-center'> <span className='inline-block mr-2 text-xs font-bold text-gray-700'>{task.created_by.firstname} {task.created_by.lastname}</span> <Avatar size={24} user={task.created_by} /> </div>
                    </div> : ''}
            </div>
        </div>
    );
}

export default CompactTodo