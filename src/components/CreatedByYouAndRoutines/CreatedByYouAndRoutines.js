import React from 'react';
import CompactTasksRenderer from '../CompactTasksRenderer/CompactTasksRenderer';
import OrangeRadio from "../../assets/radio-orange.svg"
let { createdByYouTodos } = require("../../api/todos")

function CreatedAndRoutines() {
    let byYouTodos = createdByYouTodos();
    return (
        <div className='card'>
            <div className='container'>
                <h2 className='font-bold text-lg pb-2 border-b border-gray-200'>Created By You</h2>
                <CompactTasksRenderer tasksObject={byYouTodos} />
            </div>

            <div className='container mt-10'>
                <div className='flex items-center justify-between pb-2 border-b border-gray-200'>
                    <h2 className='font-bold text-lg '>Routines</h2>
                    <img src={OrangeRadio} />
                </div>
                <CompactTasksRenderer tasksObject={byYouTodos} />
            </div>
        </div>
    );
}
export default CreatedAndRoutines