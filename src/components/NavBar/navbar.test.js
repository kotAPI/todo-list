import { render, screen } from '@testing-library/react';
import Component from './NavBar';
import { BrowserRouter as Router } from 'react-router-dom';
import "@testing-library/jest-dom/extend-expect";
// 👇️ must wrap your component in <Router>

test('Handles no props gracefully without erroring out', async () => {
    const { getByTestId } = render(
        <Router>
            <Component />,
        </Router>,
    );

    const elem = getByTestId("navbar");
    expect(elem).toBeInTheDocument();
});

test('Renders Navbar element', async () => {
    render(
        <Router>
            <Component />,
        </Router>,
    );
    const node = document.querySelector('.navbar')
    expect(node).toBeInTheDocument()
});