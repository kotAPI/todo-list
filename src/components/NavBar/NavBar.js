import React, { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom'
import "../../style/navbar.scss"
import Avatar from "../../components/UI/Avatar"
import BackArrow from "../../assets/left-arrow.svg"
const { getCurrentUser } = require("../../api/users")


function NavBar() {
    const [currentUser, setCurrentUser] = useState(getCurrentUser())
    const location = useLocation();
    const navigate = useNavigate();
    const isAtHome = () => {
        return location.pathname === "/"
    }

    function goHome() {
        navigate("/");
    }

    function GoBackHomeAction() {
        return (
            <div onClick={goHome} className="flex items-center px-4 cursor-pointer">
                <img src={BackArrow} width="10" /> <span className='ml-4 font-bold text-lg'>Home</span>
            </div>
        )
    }

    return (
        <>
            <div className='navbar ' data-testid="navbar">
                <div className='flex w-full items-center justify-between'>
                    <div>{isAtHome() ? ' ' : GoBackHomeAction()}</div>
                    <div className='flex items-center mr-2'>
                        <p className='mr-2 font-medium'>{currentUser.firstname} {currentUser.lastname}</p> <Avatar user={currentUser} /></div>
                </div>
            </div>
        </>
    );
}

export default NavBar