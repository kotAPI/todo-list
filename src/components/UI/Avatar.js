import DefaultAvatar from "../../assets/avatar-default.svg"
function Avatar({ size, user }) {
    if (!size) {
        size = 32
    }
    return (
        <div >
            {user ? <img width={size} className='rounded-full' src={user.avatar} alt="" /> : <img width={size} className='rounded-full' src={DefaultAvatar} alt="" />}
        </div>
    )
}
export default Avatar