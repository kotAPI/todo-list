function LabelRenderer({ priority }) {
    return (
        <span className="inline-block mt-1" >
            {priority === 'LOW' ? <LowLabel /> : ''}
            {priority === 'MEDIUM' ? <MediumLabel /> : ''}
            {priority === 'HIGH' ? <HighLabel /> : ''}
        </span>
    )
}

function LowLabel() {
    return (<span className="bg-blue-100 text-blue-600 text-xs py-1 px-3  rounded-md">Low</span>)
}

function MediumLabel() {
    return (<span className="bg-green-100 text-green-600 text-xs px-3 py-1  rounded-md">Medium</span>)
}

function HighLabel() {
    return (<span className="bg-red-100 text-red-600 text-xs px-3 py-1 rounded-md">High</span>)
}

export default LabelRenderer