import { render, screen } from '@testing-library/react';
import Component from './Pagination';
import { BrowserRouter as Router } from 'react-router-dom';
import "@testing-library/jest-dom/extend-expect";
// 👇️ must wrap your component in <Router>

test('Renders Pagination element', async () => {
    const { getByTestId } = render(
        <Router>
            <Component totalItems={10} currentPage={2} />,
        </Router>,
    );
    const elem = getByTestId("pagination");
    expect(elem).toBeInTheDocument();

});

test('Displays pagination right arrow', async () => {
    const { getByAltText } = render(
        <Router>
            <Component totalItems={10} currentPage={2} />,
        </Router>,
    );
    const image = getByAltText('Left Arrow Pagination');
    expect(image).toBeInTheDocument()
});

test('Displays pagination right arrow', async () => {
    const { getByAltText } = render(
        <Router>
            <Component totalItems={10} />,
        </Router>,
    );
    const image = getByAltText('Right Arrow Pagination');
    expect(image).toBeInTheDocument()
});