import LeftArrow from "../../assets/left-arrow-pagination.svg"
import RightArrow from "../../assets/right-arrow-pagination.svg"
const { getPerPage } = require("../../api/todos")
function Pagination({ currentPage = 0, totalPages = 0, totalItems = 0, pageUp, pageDown }) {
    const getShowingMin = () => {
        let perPage = getPerPage()
        return perPage * (currentPage)
    }

    const getShowingMax = () => {
        let perPage = getPerPage()
        let max = perPage * (currentPage + 1)
        if (max > totalItems) {
            max = totalItems
        }
        return max
    }

    const RightArrowRender = () => {
        let perPage = getPerPage()
        let max = perPage * (currentPage + 1)
        return (
            <>
                {max < totalItems ? <img src={RightArrow} height="20" alt="Right Arrow Pagination" className="cursor-pointer" onClick={pageUp} /> : ''}
            </>
        )
    }

    return (
        <div className="flex items-center justify-end mr-2" data-testid="pagination">
            {currentPage > 0 ? <img src={LeftArrow} height="20" className="cursor-pointer" alt="Left Arrow Pagination" onClick={pageDown} /> : ''}
            <p className="text-gray-500 text-xs px-3"> Showing <span>{getShowingMin()}</span> to <span>{getShowingMax()}</span> (of <span>{totalItems}</span> items)</p>
            <RightArrowRender />

        </div>
    )
}
export default Pagination