import CompletedIcon from "../../assets/completed.svg"
function TaskIcon({ completed, toggleTaskCompletion }) {
    const style = {
        width: '20px',
        height: '20px'
    }
    const Completed = () => {
        return (
            <img src={CompletedIcon} width="20" alt="" className="cursor-pointer" />
        )
    }
    const NotComplete =
        () => {
            return (
                <div style={style} className='border hover:border-purple-400 border-purple-200 cursor-pointer flex items-center justify-center rounded'>
                    <div className='w-3 h-3 rounded hover:bg-purple-400 bg-purple-400 p-1'></div>
                </div>
            )
        }

    return (
        <div onClick={toggleTaskCompletion}>
            {completed ? <Completed /> : <NotComplete />}
        </div>
    )
}
export default TaskIcon