import React from 'react';
import { useNavigate } from 'react-router-dom'
import CompactTodo from '../TodoItems/CompactTodo';
function CompactTasksRenderer({ tasksObject = {}, showDetails }) {
    const navigate = useNavigate();
    if (!Object.keys(tasksObject).length) {
        tasksObject = {
            todos: [],
            count: 0
        }
    }
    const todos = tasksObject.todos
    const todosCount = tasksObject.count
    const moreTodosToShow = todosCount - todos.length

    function openTodos() {
        navigate("/todos");
    }

    const NothingToShow = () => {
        return (
            <div className='h-20 flex items-center px-10 justify-center text-gray-400 text-xs'>
                -- Nothing to show --
            </div>
        )
    }

    const TasksRenderer = () => {
        return (
            <div>
                {todos.map((task) => {
                    return (<CompactTodo showDetails={showDetails} task={task} key={task.id} />)
                })}
            </div>
        )
    }

    return (
        <div>
            <div>
                {todos.length ? <TasksRenderer /> : <NothingToShow />}
            </div>
            <div>
                <div className="absolute bottom-0 left-0 px-4 py-2  flex justify-between items-center w-full text-xs border-t border-gray-200 ">
                    {moreTodosToShow > 0 ? <div className="text-gray-500 font-normal">{moreTodosToShow} more todos</div> : <div className="color-transparent">. </div>}
                    <button onClick={openTodos} className="text-purple-400">
                        View All
                    </button>
                </div>
            </div>
        </div>
    );
}
export default CompactTasksRenderer