import { render, screen } from '@testing-library/react';
import Component from './CompactTasksRenderer';
import { BrowserRouter as Router } from 'react-router-dom';
import "@testing-library/jest-dom/extend-expect";
// 👇️ must wrap your component in <Router>

test('Handles no props gracefully without erroring out', async () => {
    render(
        <Router>
            <Component />,
        </Router>,
    );
});

test('Renders Nothing To show when provided empty todos taskObject', async () => {
    let tasksObject = {
        todos: [],
        count: 0
    }
    render(
        <Router>
            <Component tasksObject={tasksObject} />,
        </Router>,
    );

    const elem = screen.queryByText('-- Nothing to show --')
    expect(elem).toBeInTheDocument()
});