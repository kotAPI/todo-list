import React from 'react';
import CompactTasksRenderer from '../CompactTasksRenderer/CompactTasksRenderer';
import GreenRadio from "../../assets/green-radio.svg"
const { todosAssignedToMe, getDueTodos } = require("../../api/todos")

function AssignedToYouAndReminders() {
    const assignedToMeRes = todosAssignedToMe();
    const dueTodos = getDueTodos();
    return (
        <div className='card'>
            <div className='container'>
                <h2 className='font-bold text-lg pb-2 border-b border-gray-200'>Assigned to you</h2>
                <CompactTasksRenderer tasksObject={assignedToMeRes} />
            </div>

            <div className='container mt-10'>
                <div className='flex items-center justify-between pb-2 border-b border-gray-200'>
                    <h2 className='font-bold text-lg'>Reminders</h2>
                    <img src={GreenRadio} />
                </div>

                <CompactTasksRenderer tasksObject={dueTodos} />
            </div>
        </div>
    );
}

export default AssignedToYouAndReminders